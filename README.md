# @universis/ngx-housing

Client library of [`@universis/housing`](https://gitlab.com/universis/housing) module.

# Usage

    npm i @universis/housing

# Configure

`@universis/ngx-housing` comes with a collection of assets which should be included in application assets

        # angular.json

        "architect": {
            "build": {
            "builder": "@angular-devkit/build-angular:browser",
            "options": {
                "outputPath": "dist",
                "index": "src/index.html",
                "main": "src/main.ts",
                "polyfills": "src/polyfills.ts",
                "tsConfig": "src/tsconfig.app.json",
                "preserveSymlinks": true,
                "assets": [
                    ...
                    {
                        "glob": ".",
                        "input": "node_modules/@universis/ngx-housing/assets",
                        "output": "assets"
                    }
                ],
                ...
