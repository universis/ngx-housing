import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdvancedFormItemResolver, AdvancedFormModalData } from '@universis/forms';
import { AdvancedListComponent } from '@universis/ngx-tables';
import { CardsListComponent } from './components/cards-list/cards-list.component';
import { HousingCardListConfiguration } from './components/housing-card-list.config';
import { HousingCardSearchConfiguration } from './components/housing-card-search.config';
import { HousingListConfiguration } from './components/housing-list.config';
import { HousingSearchConfiguration } from './components/housing-search.config';
import { EditCardComponent } from './components/edit-card/edit-card.component';
import { ModalEditCardComponent } from './components/edit-card/modal-edit-card/modal-edit-card.component';
import { EditComponent } from './components/edit/edit.component';
import { ModalEditComponent } from './components/edit/modal-edit.component';
import { ListComponent } from './components/list/list.component';
import { MessageSearchConfiguration } from './components/message-search-configuration.config';
import { MessageListConfiguration } from './components/messages-list.config';

const routes: Routes = [
    {
        'path': '',
        'pathMatch': 'full',
        'redirectTo': 'index'
    },
    {
        path: 'index',
        component: ListComponent,
        data: {
            model: 'HousingRequestActions',
            description: 'NewRequestTemplates.HousingRequestAction.Description',
            tableConfiguration: HousingListConfiguration,
            searchConfiguration: HousingSearchConfiguration
        },
        children: [
          {
            path: 'item/:id/edit',
            component: ModalEditComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'HousingRequestActions',
              action: 'modal-edit',
              serviceQueryParams: EditComponent.ServiceQueryParams,
              closeOnSubmit: true,
              continueLink: '.'
            },
            resolve: {
              model: AdvancedFormItemResolver
            }
          }
        ]
    },
    {
      path: 'housing-cards',
      component: CardsListComponent,
      data: {
          model: 'StudentHousingCards',
          description: 'UniversisHousingModule.HousingCardsTemplates.HousingCard.Description',
          tableConfiguration: HousingCardListConfiguration,
          searchConfiguration: HousingCardSearchConfiguration 
      },
      children: [
        {
          path: 'item/:id/edit',
          component: ModalEditCardComponent,
          outlet: 'modal',
          data: <AdvancedFormModalData>{
            model: 'StudentHousingCards',
            action: 'modal-edit',
            serviceQueryParams: EditCardComponent.ServiceQueryParams,
            closeOnSubmit: true,
            continueLink: '.'
          },
          resolve: {
            model: AdvancedFormItemResolver
          }
        }
      ]
  },
  {
    path: 'messages',
    component: AdvancedListComponent,
    data: {
        model: 'HousingRequestActionMessages',
        description: 'UniversisHousingModule.Messages.Title',
        tableConfiguration: MessageListConfiguration,
        searchConfiguration: MessageSearchConfiguration
    },
    children: [
      {
        path: 'item/:id/edit',
        
        component: ModalEditComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          model: 'HousingRequestActions',
          serviceQueryParams: EditComponent.ServiceQueryParams,
          action: 'modal-edit',
          closeOnSubmit: true,
          continueLink: '.'
        },
        resolve: {
          model: AdvancedFormItemResolver
        }
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AdminHousingRoutingModule { }
