import { NgModule } from "@angular/core";
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { HOUSING_LOCALES } from './i18n/index';

@NgModule({
    declarations: [],
    imports: [
        TranslateModule
    ],
    exports: []
})

export class HousingSharedModule {
    constructor(private _translateService: TranslateService) {
        this.ngOnInit().catch(err => {
            console.log('An error occurred while loading housing shared module');
            console.error(err);
        });
    }  

    static forRoot() {
        return {
            ngModule: HousingSharedModule
        };
    }

    async ngOnInit() {
        Object.keys(HOUSING_LOCALES).forEach(lang => {
            if (HOUSING_LOCALES.hasOwnProperty(lang)) {
            this._translateService.setTranslation(lang, HOUSING_LOCALES[lang], true);
            }
        });
    }
}