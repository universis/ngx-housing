import { el } from './housing.el';
import { en } from './housing.en';

export const HOUSING_LOCALES = {
  el,
  en
};
