/*
 * Public API Surface of housing
 */

export * from './lib/housing.service';
export * from './lib/housing.component';
export * from './lib/housing.module';
export * from './lib/housing-routing.module';
